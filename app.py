import json
import urllib3
import re
from models import *
from classes.Schedules import Schedules
from classes.SchedulesItem import SchedulesItem

classes = []

untitle = lambda s: s[:1].lower() + s[1:] if s else ''


def create_list_class(name, data):
    item = data[0]
    if type(item) == list:
        create_list_class(name, item)
    elif type(item) == dict:
        create_class(name, item)


def create_class(name, data):
    _properties = 'class ' + name + ':\n    '
    _methods = '    def __init__(self, _data):' + '\n        '
    _imports = ''
    for item in data:
        if type(data[item]) == list:
            _properties += untitle(str(item)) + ' = []  # ' + name + 'Item' + '\n    '
            _methods += 'self.' + untitle(str(item)) + \
                        ' = [' + name + 'Item]*len(_data[\"' + str(item) + '\"])' + '\n        '
            _methods += 'i = 0' + '\n        '
            _methods += 'for item in _data[\"' + str(item) + '\"]:' + '\n        '
            _methods += '    self.' + untitle(str(item)) + '[i] = ' + name + 'Item(item)' + '\n        '
            _methods += '    i += 1' + '\n        '
            _imports += 'from classes.' + name + 'Item import *' + '\n'
            create_list_class(name + 'Item', data[item])
        elif type(data[item]) == dict:
            _properties += untitle(str(item)) + ' = ' + str(item).title() + '\n    '
            create_class(str(item).title(), data[item])
        else:
            if isinstance(data[item], type(None)):
                _properties += untitle(str(item)) + ' = ' + str('None') + '\n    '
            else:
                _properties += untitle(str(item)) + ' = ' + str(type(data[item]))[8:-2] + '\n    '
            _methods += 'self.' + untitle(str(item)) + ' = ' + str(type(data[item]))[8:-2] + \
                        '(_data[\"' + str(item) + '\"])' + '\n        '
    if len(_imports) > 1:
        _imports += '\n'
    _class = _imports + '\n' + _properties + '\n' + _methods
    _class = _class.strip('    ') + '\n'

    classes.append(_class)


def create_class_files(name, js):
    create_class(name, js)
    for class_item in classes:
        file_name = re.match('^([\s\S]+)(class )(.+)(:)', class_item)
        f = open('.\\classes\\' + file_name.group(3) + '.py', 'w')
        f.writelines(class_item)


print(get_schedules(2015))
# create_class_files('Schedules', get_schedules(2015))

sch = Schedules(get_schedules(2015))
for schedule in sch.data:
    print(schedule.description, ' : ', schedule.percentage)
