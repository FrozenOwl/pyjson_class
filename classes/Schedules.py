from classes.SchedulesItem import *


class Schedules:
    data = []  # SchedulesItem
    count = int
    
    def __init__(self, _data):
        self.data = [SchedulesItem]*len(_data["Data"])
        i = 0
        for item in _data["Data"]:
            self.data[i] = SchedulesItem(item)
            i += 1
        self.count = int(_data["Count"])

