
class SchedulesItem:
    checkupCount = int
    description = str
    endPeriodDate = str
    closedCount = int
    employeeCount = int
    percentage = int
    annulatoCount = int
    scheduleItemId = str
    startPeriodDate = str
    closingCount = int
    
    def __init__(self, _data):
        self.checkupCount = int(_data["CheckupCount"])
        self.description = str(_data["Description"])
        self.endPeriodDate = str(_data["EndPeriodDate"])
        self.closedCount = int(_data["ClosedCount"])
        self.employeeCount = int(_data["EmployeeCount"])
        self.percentage = int(_data["Percentage"])
        self.annulatoCount = int(_data["AnnulatoCount"])
        self.scheduleItemId = str(_data["ScheduleItemId"])
        self.startPeriodDate = str(_data["StartPeriodDate"])
        self.closingCount = int(_data["ClosingCount"])

