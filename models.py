import urllib3
import json

http = urllib3.PoolManager()


def get_schedules(year):
    r = http.request(url='http://insp.skat.local/inspection/Schedule/GetSheduleItemsMin/'
                         '52299b81-3854-4d34-bf37-ef35bf1d1ea9/'+str(year), method='GET')
    return json.loads(r.data.decode('utf-8'))


def get_schedule_by_month(month_id):
    r = http.request(url='http://insp.skat.local/inspection/Schedule/GetCheckupsList/', method='GET')
    return r.data.decode('utf-8')
